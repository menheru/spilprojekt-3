using UnityEngine;

public class TurnTable : MonoBehaviour
{
    public int framesPerSecond = 10;

    public int frames = 60;

    public GameObject model;

    private void Start()
    {
        Instantiate(model, transform);
        model.transform.parent = transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, Time.deltaTime * framesPerSecond % frames, 0));
    }
}
