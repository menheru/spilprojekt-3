using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinManager : MonoBehaviour
{
    public void AddNewSin()
    {
        // Gain random powerdown
        //Powerdown[] childs = sinsContainer.GetComponentsInChildren<Powerdown>();

        Debug.Log("Child objects: " + sinsContainer.transform.childCount);

        Powerdown[] availableSins = new Powerdown[sinsContainer.transform.childCount];
        int i = 0;
        foreach (Transform child in sinsContainer.transform)
        {
            Powerdown pd = child.gameObject.GetComponent<Powerdown>();

            if (pd == null)
            {
                Debug.LogWarning("Sin is missing Powerdown script: " + child.gameObject.name);
                continue;
            }

            if (!child.gameObject.activeInHierarchy)
            {
                availableSins[i] = pd;
            }
            i++;
        }

        Debug.Log("Found " + availableSins.Length + " available sins");

        // Get a random action powerdown
        if (availableSins.Length > 0)
        {
            Powerdown finalPowerdown = availableSins[Random.Range(0, availableSins.Length)];
            finalPowerdown.gameObject.SetActive(true);

            Debug.Log("Final Sin: " + finalPowerdown.PowerdownName);

            // Display a popup on screen
            GameManager.instance.SetStateToBackstory(finalPowerdown.backstory);
            finalPowerdown.EnablePowerdown();
        }
    }
}
