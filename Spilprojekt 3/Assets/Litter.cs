using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Litter : MonoBehaviour
{
    [SerializeField]
    private Vector2 minMaxObjects = new Vector2(4, 7);

    [Range(0, 1)]
    private float chanceOfSpawning = 0.3f;

    [SerializeField]
    private GameObject[] trashObjects;

    private void Start()
    {
        ItsTrashTime();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("car"))
        {
            ItsTrashTime();
        }
    }

    private void ItsTrashTime()
    {
        // Random.value generates a random value from 0 to 1 (inclusive)
        if (Random.value <= chanceOfSpawning)
        {
            int amount = (int)Random.Range(minMaxObjects.x, minMaxObjects.y + 1);

            for (int i = 0; i < amount; i++)
            {
                Vector3 rndPosition = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f));
                rndPosition = transform.TransformPoint(rndPosition * .5f);
                rndPosition.y = 0.5f;
                Instantiate(trashObjects[0], rndPosition, transform.rotation);
            }
        }
    }
}
