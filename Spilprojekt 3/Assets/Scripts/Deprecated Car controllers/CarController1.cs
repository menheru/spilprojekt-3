using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController1 : MonoBehaviour
{
    private Rigidbody sphere;
    
    // public Transform kartModel;
    [Header("Parameters")]
    [SerializeField][Tooltip("Gravity bruh")]
    private float gravity;
    [SerializeField][Tooltip("Speed goal, when driving forward")]
    private float topSpeed; // Speed aimed at when driving forward
    private float speed, currentSpeed; // Speed is target speed, currentSpeed is lerped speed.
    

    void Awake(){
        sphere = GetComponentInChildren<Rigidbody>();
    }

    void Update()
    {

        transform.position = sphere.transform.position; 

        Acceleration();
       // Animation();
    
   
        // Follow sphere
    
    }
    
    void Acceleration(){
        float moveInput = Input.GetAxisRaw("Vertical");
        if(moveInput > 0)
            speed = topSpeed;
        currentSpeed = Mathf.SmoothStep(currentSpeed, speed, Time.deltaTime * 12f); speed = 0f;
    
    }

    void Animation(){
        /*b) Wheels
        frontWheels.localEulerAngles = new Vector3(0, (Input.GetAxis("Horizontal") * 15), frontWheels.localEulerAngles.z);
        frontWheels.localEulerAngles += new Vector3(0, 0, sphere.velocity.magnitude/2);
        backWheels.localEulerAngles += new Vector3(0, 0, sphere.velocity.magnitude/2);*/
    }


    void FixedUpdate(){
        
        // Drive Acceleration
        sphere.AddForce(transform.forward * currentSpeed, ForceMode.Acceleration);

        // Gravity
        sphere.AddForce(Vector3.down * gravity, ForceMode.Acceleration);    
    }
}