using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CarController3 : MonoBehaviour
{
    private Rigidbody rb;
    

    [Header("Car stats")]
    [SerializeField]
    private float maxVelocity;
    [SerializeField]
    private float forwardForce;

    void Awake(){
        rb = GetComponent<Rigidbody>();
    }

    void Update(){
        if(Input.GetKey(KeyCode.Space))
            Drive();
    }

    public void Drive(){
        rb.AddForce(forwardForce * transform.forward, ForceMode.Acceleration);
        if(rb.velocity.magnitude < maxVelocity) 
            rb.velocity = rb.velocity.normalized * maxVelocity;

    }




}
