using System;
using UnityEngine;

public class CarController2 : MonoBehaviour
{
    private const string HORI = "Horizontal";
    private const string VERT = "Vertical";
    private Rigidbody carRB; 

    private float horiInput;
    private float vertInput;
    private float currentSteeringAngle;
    private bool isBreaking;
    private float currentBreakForce;
    
    [Header("Car stats")]
    [SerializeField][Tooltip("Force applied to motor torque, multiplied by vertical input")] private float motorForce;
    [SerializeField][Tooltip("Force applied to break torque")] private float breakForce;
    [SerializeField] private float maxSteeringAngle;
    [SerializeField][Tooltip("Drag to stop car when not driving")] private float drag;

    [Header("Wheel Colliders and Transforms")]
    [SerializeField][Tooltip("Front left wheel collider")] private WheelCollider frontLeftWheelCollider;
    [SerializeField][Tooltip("Front right wheel collider")] private WheelCollider frontRightWheelCollider;
    [SerializeField][Tooltip("Back left wheel collider")] private WheelCollider backLeftWheelCollider;
    [SerializeField][Tooltip("Back right wheel collider")] private WheelCollider backRightWheelCollider;
    
    [SerializeField][Tooltip("Front left wheel transform")] private Transform frontLeftWheelTransform;
    [SerializeField][Tooltip("Front right wheel transform")] private Transform frontRightWheelTransform;
    [SerializeField][Tooltip("Back left wheel transform")] private Transform backLeftWheelTransform;
    [SerializeField][Tooltip("Back right wheel transform")] private Transform backRightWheelTransform;
    
    [Header("Press key to")]
    [SerializeField][Tooltip("Key to press for breaking")]
    private KeyCode breakKey;

    void Awake(){
        carRB = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        GetInput();
        HandleMotor();
        HandleSteering();
        UpdateWheels();
    }

    private void GetInput(){
        horiInput = Input.GetAxis(HORI);
        vertInput = Input.GetAxisRaw(VERT);
        isBreaking = Input.GetKey(breakKey);
    }

    private void HandleMotor(){
        
        // Add forward force to front wheels
        frontLeftWheelCollider.motorTorque = vertInput * motorForce * Time.deltaTime;
        frontRightWheelCollider.motorTorque = vertInput * motorForce * Time.deltaTime;

        if(vertInput == 0)
            carRB.drag = drag;
        else carRB.drag = 0;

        // Add force to breaks
        currentBreakForce = isBreaking ? breakForce : 0f;
        Break();
    }

    private void Break(){
        frontLeftWheelCollider.brakeTorque = currentBreakForce;
        frontRightWheelCollider.brakeTorque = currentBreakForce;
        backLeftWheelCollider.brakeTorque = currentBreakForce;
        backRightWheelCollider.brakeTorque = currentBreakForce;
    }

    private void HandleSteering(){
        currentSteeringAngle = maxSteeringAngle * horiInput;
        frontLeftWheelCollider.steerAngle = currentSteeringAngle;
        frontRightWheelCollider.steerAngle = currentSteeringAngle;
    }

    private void UpdateWheels(){
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheelTransform);
        UpdateSingleWheel(backLeftWheelCollider, backLeftWheelTransform);
        UpdateSingleWheel(backRightWheelCollider, backRightWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform){
        Vector3 pos;
        Quaternion rot;
        wheelCollider.GetWorldPose(out pos, out rot);
        wheelTransform.position = pos;
        wheelTransform.rotation = rot;
    }
}
