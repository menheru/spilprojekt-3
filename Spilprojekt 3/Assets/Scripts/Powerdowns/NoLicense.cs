using UnityEngine;

public class NoLicense : Powerdown
{
    public CarController car;
    private KeyCode defaultBrakeKey;
    [SerializeField]
    public KeyCode[] randomKeys;
    [SerializeField]
    private KeyCode finalKey;

    public override void DisablePowerdown()
    {
        car.brakeKey = defaultBrakeKey;
    }

    public override void EnablePowerdown()
    {
        car.brakeKey = finalKey;
    }

    public override void RegisterPowerdown()
    {
        PowerdownName = "Driving without License";
        Description = "Forgot to take your drivers license? too bad now you cant find the brake key";

        finalKey = randomKeys[Random.Range(0, randomKeys.Length)];
    }
}
