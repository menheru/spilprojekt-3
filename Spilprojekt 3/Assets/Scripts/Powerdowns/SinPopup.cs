using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SinPopup : MonoBehaviour
{
    public Text mainText;
    public Text powerdownText;
    public Text descriptionText;

    public GameObject sinsContainer;

    public void AddNewSin()
    {
        // Gain random powerdown
        //Powerdown[] childs = sinsContainer.GetComponentsInChildren<Powerdown>();

        Debug.Log("Child objects: " + sinsContainer.transform.childCount);

        Powerdown[] availableSins = new Powerdown[sinsContainer.transform.childCount];
        int i = 0;
        foreach (Transform child in sinsContainer.transform)
        {
            Powerdown pd = child.gameObject.GetComponent<Powerdown>();

            if (pd == null)
            {
                Debug.LogWarning("Sin is missing Powerdown script: " + child.gameObject.name);
                continue;
            }

            if (!child.gameObject.activeInHierarchy)
            {
                availableSins[i] = pd;
            }
            i++;
        }

        Debug.Log("Found " + availableSins.Length + " available sins");

        // Get a random action powerdown
        if (availableSins.Length > 0)
        {
            Powerdown finalPowerdown = availableSins[Random.Range(0, availableSins.Length)];
            finalPowerdown.gameObject.SetActive(true);

            Debug.Log("Final Sin: " + finalPowerdown.PowerdownName);

            // Display a popup on screen
            StartCoroutine(ReceivedPowerdown(finalPowerdown));
        }
    }

    IEnumerator ReceivedPowerdown(Powerdown pd)
    {
        ShowPopup(pd);

        // Play some cool particles from a script on poptext object
        // and a small tune.

        yield return new WaitForSeconds(3);

        RemovePopup();
    }

    public void ShowPopup(Powerdown powerdown)
    {
        mainText.gameObject.SetActive(true);
        descriptionText.gameObject.SetActive(true);
        powerdownText.gameObject.SetActive(true);

        descriptionText.text = powerdown.Description;
        powerdownText.text = powerdown.PowerdownName;
    }

    public void RemovePopup()
    {
        mainText.gameObject.SetActive(false);
        powerdownText.gameObject.SetActive(false);
        descriptionText.gameObject.SetActive(false);
    }
}
