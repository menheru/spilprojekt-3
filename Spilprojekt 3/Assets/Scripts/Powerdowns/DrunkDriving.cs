using UnityEngine;

public class DrunkDriving : Powerdown
{
    public CarController car;

    public override void RegisterPowerdown()
    {
        car = GameObject.FindGameObjectWithTag("car").GetComponent<CarController>();
        PowerdownName = "Drunk Driving";
        Description = "Your WASD keys will be inverted, meaning W = S and A = D and vice versa.";
    }

    public override void EnablePowerdown()
    {
        car.InvertedControls = true;
    }

    public override void DisablePowerdown()
    {
        car.InvertedControls = false;
    }
}
