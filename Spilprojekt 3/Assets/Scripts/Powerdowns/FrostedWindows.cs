using UnityEngine.UI;

public class FrostedWindows : Powerdown
{
    public Image frostedTintUI; // reference til et image, som ligger over spillets view

    public override void RegisterPowerdown()
    {
        PowerdownName = "Frosted Windows";
        Description = "Your screen will be blocked, just a little bit by a pretty frosted overlay.";
    }

    public override void DisablePowerdown()
    {

        frostedTintUI.gameObject.SetActive(false);
    }

    public override void EnablePowerdown()
    {
        frostedTintUI.gameObject.SetActive(true);
    }
}
