using UnityEngine;

public class StealingTires : Powerdown
{
    [SerializeField] private float acceleration = 75;
    [SerializeField] private CarController car;
    private float defaultAcceleration;

    public override void DisablePowerdown()
    {
        car.acceleration = defaultAcceleration;
    }

    public override void EnablePowerdown()
    {
        car.acceleration = acceleration;
    }

    public override void RegisterPowerdown()
    {
        PowerdownName = "Stealing Tires";
        Description = "Your speed is permanently decreased, just end your suffering already :)";
        defaultAcceleration = car.acceleration;
    }
}
