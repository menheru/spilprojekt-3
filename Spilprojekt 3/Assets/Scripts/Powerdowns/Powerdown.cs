using UnityEngine;

public abstract class Powerdown : MonoBehaviour
{
    public Backstory backstory;

    private string _name = "unnamed powerdown";
    public string PowerdownName { get => _name; set => _name = value; }

    private string _description = "description";
    public string Description { get => _description; set => _description = value; }

    private void Awake()
    {
        RegisterPowerdown();

        Debug.Log("Registered Powerdown: " + _name);
    }

    private void OnEnable()
    {
        if(backstory != null) GameManager.instance.SetStateToBackstory(backstory);

        EnablePowerdown();

        Debug.Log("Enabled Powerdown: " + _name);
    }

    private void OnDisable()
    {
        DisablePowerdown();

        Debug.Log("Disabled Powerdown: " + _name);
    }

    public abstract void RegisterPowerdown();
    public abstract void EnablePowerdown();
    public abstract void DisablePowerdown();
}
