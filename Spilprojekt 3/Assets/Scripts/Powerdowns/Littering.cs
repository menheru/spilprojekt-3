using UnityEngine;

public class Littering : Powerdown
{
    [SerializeField]
    private GameObject litterSpotsContainer;

    public override void DisablePowerdown()
    {
        litterSpotsContainer.SetActive(false);
    }

    public override void EnablePowerdown()
    {
        litterSpotsContainer.SetActive(true);
    }

    public override void RegisterPowerdown()
    {
        PowerdownName = "Littering";
        Description = "Look out! there might be trash on the road, thanks to yourself!";
    }
}
