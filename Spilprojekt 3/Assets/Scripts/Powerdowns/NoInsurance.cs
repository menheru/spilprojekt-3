using UnityEngine;

public class NoInsurance : Powerdown
{
    public CarLives lives;

    public override void DisablePowerdown()
    {
        lives.InstantDeath = false;
    }

    public override void EnablePowerdown()
    {
        lives.InstantDeath = true;
    }

    public override void RegisterPowerdown()
    {
        PowerdownName = "No Insurance";
        Description = "With no Insurance there is no lives, and you will just instantly respawn at the start.";
    }
}
