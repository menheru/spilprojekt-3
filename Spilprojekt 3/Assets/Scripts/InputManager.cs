using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null) { Instance = this; }
    }

    private void Start()
    {
        ResetControls();
    }

    [SerializeField]
    private KeyCode _acceleration = KeyCode.W;
    public KeyCode Acceleration { get; set; }

    [SerializeField]
    private KeyCode _turnLeft = KeyCode.A;
    public KeyCode TurnLeft { get; set; }

    [SerializeField]
    private KeyCode _turnRight = KeyCode.D;
    public KeyCode TurnRight { get; set; }

    [SerializeField]
    private KeyCode _backwards = KeyCode.S;
    public KeyCode Backwards { get; set; }

    [SerializeField]
    private KeyCode _brakeKey = KeyCode.Space;
    public KeyCode Brake { get; set; }


    public void ResetControls()
    {
        Acceleration = _acceleration;
        TurnLeft = _turnLeft;
        TurnRight = _turnRight;
        Backwards = _backwards;
        Brake = _brakeKey;
    }
}
