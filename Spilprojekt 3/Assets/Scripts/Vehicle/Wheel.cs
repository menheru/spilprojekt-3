using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Create Wheel", menuName = "Vehicle/Wheel")]
public class Wheel : ScriptableObject
{
    public new string name;
    public float groundDrag;
}
