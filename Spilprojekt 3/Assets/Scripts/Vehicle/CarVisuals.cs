using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarVisuals : MonoBehaviour
{
    [Header ("References")]
    public CarController cController;
    public ParticleSystem[] carDust;
    public TrailRenderer[] carTrail;
    public TrailRenderer[] carTrailDark;

    [Header ("Dust Contrails")]
    [Range(0, 1)] public float CarDustCutoff = 0.5f;
    [Tooltip ("This is just equal to the car's acceleration")]public float SpeedMax;
    [Range (0,1)] float emissionAmount;

    [Header("Engine Pitching")]
    [Range(0, 1)] public float EngineCutoff = 0.5f;
    [Range(0, 1)] float speedNormalized;
    public AudioSource engineSound;
    
    public float PitchMin = 0.5f;
    public float PitchMax = 2f;
    float PitchTarg;

    [Header("Tire Screeching")]
    [Range(0, 1)] public float ScreechCutoff = 0.5f;
    [Tooltip("This is just equal to the car's maxSteer")] public float SteerMax;
    [Tooltip("This is just equal to the car's minSteer")] public float SteerMin;
    public float SteerCurrent;
    public float SteerNomalized;
    public AudioSource tireScreech;

    public float VolMin = 0f;
    public float VolMax = 1f;
    public float VolTarg;

    [Header("FOV Change")]
    public Camera cam;
    public float FovMin;
    public float FovMax;
    public float FovTarg;
    



    //debugs
    public float steerDebug;
    public float debugInput;

    void Update()
    {
        #region Dust Particle Amount

        emissionAmount = cController.CurrentSpeed / SpeedMax;
        if(emissionAmount > CarDustCutoff)
        {
            var psystem = carDust[0].emission;
            psystem.rateOverTime = emissionAmount * 40;
            var psystem1 = carDust[1].emission;
            psystem1.rateOverTime = emissionAmount * 40;
        }
        else
        {
            var psystem = carDust[0].emission;
            psystem.rateOverTime = 0;
            var psystem1 = carDust[1].emission;
            psystem1.rateOverTime = 0;
        }
        #endregion

        #region Engine Pitch
        speedNormalized = cController.CurrentSpeed / SpeedMax;
        PitchTarg = Mathf.Lerp(PitchMin, PitchMax, speedNormalized);
        engineSound.pitch = PitchTarg;
        #endregion

        
        #region Tire Screech
        SteerCurrent = cController.CurrentSteer;
        SteerNomalized = SteerCurrent / (SteerMax-SteerMin);
        VolTarg = Mathf.Lerp(VolMin, VolMax, SteerNomalized);
        Debug.Log(SteerNomalized);


        #endregion

        #region fovChange
        FovTarg = Mathf.Lerp(FovMin, FovMax, speedNormalized);
        Mathf.Clamp(cam.fieldOfView, FovMin, 74);
        cam.fieldOfView = FovTarg;

        #endregion

    }
}
