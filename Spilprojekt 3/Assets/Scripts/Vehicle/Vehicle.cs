using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Create Vehicle", menuName = "Vehicle/Vehicle")]
public class Vehicle : ScriptableObject
{
    public new string name;

    public float acceleration;
    public float breakForce;
    public float maxSpeed;
    public float turnStrength;
}
