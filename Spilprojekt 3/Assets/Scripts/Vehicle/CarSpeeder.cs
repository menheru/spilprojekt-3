using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpeeder : MonoBehaviour
{

    private Rigidbody rb;
    public GameObject SpawnPoint;

    public GameObject[] cars;

    private float n1;
    private float n2;
    private float n3;
    private int n4;

    // Update is called once per frame

    private void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        n1 = Random.Range(.8f,1);
        n2 = Random.Range(-.2f, 1);
        n3 = Random.Range(500,1000);
        n4 = Random.Range(0, cars.Length);
        cars[n4].SetActive(true);

    }

    void Update()
    {
        cars[n4].SetActive(true);
        rb.AddForce(new Vector3(n2,0,n1) * n3 * Time.deltaTime, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "killbox")
        {
            Destroy(transform.parent.gameObject);
        }
    }
}
