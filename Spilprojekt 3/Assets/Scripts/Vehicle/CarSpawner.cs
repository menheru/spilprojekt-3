using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{

    public GameObject Car;
    public GameObject SpawnPoint;

    public float spawnerTimerMax;
    public float spawnerTimerMin;

    private int carNum;

    public int floodchance;
    private int floodchance_;

    private bool looping = true;

    void Start()
    {
        StartCoroutine("CarSpawnerTimer");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SpawnCar();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            Flood();
        }      
    }

    void SpawnCar()
    {
        Instantiate(Car, SpawnPoint.transform.position, SpawnPoint.transform.rotation);
    }

    public IEnumerator CarSpawnerTimer()
    {
        while (looping)
        {
            floodchance_ = Random.Range(0, floodchance);
            if (floodchance_ == floodchance)
                Flood();
            else
                SpawnCar();
            yield return new WaitForSeconds(Random.Range(spawnerTimerMin, spawnerTimerMax));
        }
    }

    void Flood()
    {
        for (int i = 0; i < 100; i++)
        {
            SpawnCar();
        }
    }
}
