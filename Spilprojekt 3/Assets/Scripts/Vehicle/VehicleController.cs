using UnityEngine;

public class VehicleController : MonoBehaviour
{
    [Header("Vehicle Stats")]
    public float acceleration = 8.0f;
    public float breakForce = 4.0f;
    public float maxSpeed = 10.0f;
    public float turnStrength = 180.0f;
    public bool invertedControls = false;

    // Wheel Settings
    [Header("Wheel")]
    public float groundDrag = 2.0f;

    public Rigidbody rb;

    public float gravity = 10;

    private float speedInput, turnInput;

    private bool grounded;
    public float rayLength = .5f;
    public LayerMask ground;
    public Transform groundRayPoint;

    public Transform leftFrontWheel, rightFrontWheel;
    private float maxWheelTurn = 25.0f;

    public Transform[] wheels;

    void Start()
    {
        rb.transform.parent = null;
    }

    void Update()
    {
        int invert = invertedControls ? -1 : 1;

        speedInput = 0.0f;
        float verticalInput = Input.GetAxis("Vertical") * invert;

        if(verticalInput > 0)
        {
            speedInput = verticalInput * acceleration * 1000;
        }else if(verticalInput < 0)
        {
            speedInput = verticalInput * breakForce * 1000;
        }

        turnInput = Input.GetAxis("Horizontal");

        if (grounded)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, turnInput * verticalInput * turnStrength * Time.deltaTime));
        }

        leftFrontWheel.localRotation = Quaternion.Euler(leftFrontWheel.localRotation.eulerAngles.x, (turnInput * maxWheelTurn) - 180, leftFrontWheel.localRotation.eulerAngles.z);
        rightFrontWheel.localRotation = Quaternion.Euler(rightFrontWheel.localRotation.eulerAngles.x, turnInput * maxWheelTurn, rightFrontWheel.localRotation.eulerAngles.z);

        transform.position = rb.transform.position;
    }

    void FixedUpdate()
    {
        RaycastHit hit;
        grounded = Physics.Raycast(groundRayPoint.position, -transform.up, out hit, rayLength, ground);

        if (grounded)
        {
            Debug.DrawLine(groundRayPoint.position, hit.point, Color.green);

            transform.rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;

            rb.drag = groundDrag;

            if (Mathf.Abs(speedInput) > 0)
            {
                rb.AddForce(transform.forward * speedInput);

                //foreach(Transform wheelTrans in wheels)
                //{
                //    wheelTrans.Rotate(new Vector3(wheelTrans.rotation.x, ((Mathf.PI * 2) * rb.velocity.magnitude)  * Mathf.Clamp(speedInput, -1, 1), wheelTrans.rotation.z));
                //}

                // Play particles
                // Play sounds
            }
        }
        else
        {
            Debug.DrawLine(groundRayPoint.position, hit.point, Color.red);
            rb.drag = 0.1f;
            rb.AddForce(Vector3.up * -gravity * 100);
        }
    }
}
