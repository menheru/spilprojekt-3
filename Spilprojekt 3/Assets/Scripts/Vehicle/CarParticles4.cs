using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarParticles4 : MonoBehaviour
{    

    [Header("Particles")]
    [SerializeField]
    private List<ParticleSystem> driftParticles = new List<ParticleSystem>();

    [SerializeField] private Color[] turboColors;


    bool first, second, third;
    Color c;
    int driftMode;

    public int ColorDrift(float driftPower)
    {
        if(!first)
            c = Color.clear;

        if (driftPower > 50 && driftPower < 100-1 && !first)
        {
            first = true;
            c = turboColors[0];
            driftMode = 1;
        }

        if (driftPower > 100 && driftPower < 150- 1 && !second)
        {
            second = true;
            c = turboColors[1];
            driftMode = 2;
        }

        if (driftPower > 150 && !third)
        {
            third = true;
            c = turboColors[2];
            driftMode = 3;
        }

        foreach (ParticleSystem p in driftParticles)
        {
            var pmain = p.main;
            pmain.startColor = c;
        }
        return driftMode;
    }

    public void StartDriftParticles(){ 
        foreach (ParticleSystem p in driftParticles)
        {
            p.Play();
        }
    }

    public void StopDriftParticles(){ 
        foreach (ParticleSystem p in driftParticles)
        {
            p.Stop();
        }
    }




}
