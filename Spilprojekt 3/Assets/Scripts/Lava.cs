using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    [Tooltip("The Gameobject that has carcontroller and carlives component")]
    private GameObject carGameObject;
    private CarController cc;
    private CarLives carLives;
    [SerializeField] private int carDamage = 1;
    [SerializeField] private Backstory lavaBackstory;

    void Awake(){
        carGameObject = GameObject.FindWithTag("carcomponentholder");
        cc = carGameObject.GetComponent<CarController>();
        carLives = carGameObject.GetComponent<CarLives>();
    }

    void OnTriggerEnter(Collider collision)
    {
        if(collision.CompareTag("car"))
        {
            cc.StopTyreMark(true);

            if (lavaBackstory != null)
                GameManager.instance.SetStateToBackstory(lavaBackstory);
            else
                Debug.Log("No win backstory set in " + this.gameObject.name);
            
            LapManager.instance.ResetCheckpoints();
            SoundManager.instance.PlayLavaSound();
            carLives.LivesDecrease(carDamage, true);
        }
    }

    public void Initialize (CarLives _carLives) {
        carLives = _carLives;
    }
}
