using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private CarLives carLives;
    [SerializeField] private int carDamage = 1;
    
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if(collision.gameObject.tag == "car")
        {
            carLives.LivesDecrease(carDamage);
        }
    }

    public void Initialize (CarLives _carLives){
        carLives = _carLives;
    }
}
