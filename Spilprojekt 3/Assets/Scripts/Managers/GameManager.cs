using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    public static GameManager instance;

    void Awake(){
        if(instance == null)
            instance = this;
        else Debug.LogWarning("big cry, 1< game manager");

        cc = FindObjectOfType<CarController>();
    }

    #endregion

    /// Other manager references //////
    private BackstoryManager backstoryManager;  // Class for playing back stories    
    private LapManager lapManager;              // Class for laps
    private CarLives carLives;                  // Class for lives

    /// Variables  ///////////////////
    private GameObject currentCar;

    private CarController cc;

    [SerializeField] private List<CarReferences> cars;

    private Queue<Backstory> backstories = new Queue<Backstory>();

    private void Start()
    {
        lapManager = LapManager.instance;
        cc.UpdateCar(ShuffleCars());
    }
    
    
    ////  CARS!!! /////////////////////////////////////////////////////////////////////////////////////
    public CarReferences ShuffleCars()
    {
        int rnd = Random.Range(0, cars.Count);
        return cars[rnd];
    }

    /// GAME STATES  ////////////////////////////////////////////////////////////////////

    public enum GameState
    {
        Started,
        Backstory,
        Paused, 
    }
    
    private GameState currentState;
    public GameState CurrentState {
        get{return currentState; }  
    }


    // Function for starting game when game is paused or after backstory has displayed.
    public bool SetStateToStarted()
    {
        if(currentState != GameState.Started) {
            // If backstory in queue, play that first.
            currentState = GameState.Started;
            if(backstories.Count > 0) {
                SetStateToBackstory(backstories.Dequeue());
                return false;
            }

            Time.timeScale = 1;
            return true;}
        else {
            Debug.LogWarning("Game already running");
            return false;
        }
    }

    // Cannot pause game if already paused or in a backstory.
    public bool SetStateToPaused()
    {
        if(currentState == GameState.Started)
        {
            Time.timeScale = 0;
            currentState = GameState.Paused;
            return true;
        }
        else {
            Debug.LogWarning("Game CANNOT be paused when it's not running, that means it is already paused. Backstories cannot be paused either.");
            return false;    
        }
    }
    
    // Takes a Backstory object and displays it as a "shortfilm"
    public void SetStateToBackstory(Backstory _backstory)
    {
        
        // If game is paused or already displaying a backstory, add the
        // Backstory to a queue so it will play after pause/displayingBackstory
        if(currentState != GameState.Started)
        {
            backstories.Enqueue(_backstory);
            return;
        }    
        else
        {
            Time.timeScale = 0;
            GameObject backstoryGameObject = Instantiate(_backstory.BackstoryParent);
            BackstoryMonobehaviour bm = backstoryGameObject.GetComponent<BackstoryMonobehaviour>();
            
            // Play event
            if(_backstory.Event != null){
                if(_backstory.playBefore){
                    if(_backstory.dontPause)
                        Time.timeScale = 1;
                    _backstory.Event.Raise();
                }
                else
                    bm.SetVoidEvent(_backstory.Event);        
            }
            // Load scene
            if(_backstory.LoadSceneAfterBackstory){
                bm.SetLoadScene(_backstory.SceneName);
            }

            currentState = GameState.Backstory;
        }
    }
    // GAME STATES END ////////////////////////////////////////////////////////////////////////////////////////////////////

    
}
