using UnityEngine.SceneManagement;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    public void LoadSceneByName(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
    public void LoadSceneByIndex(int sceneIndex){
        SceneManager.LoadScene(sceneIndex);
    }
}
