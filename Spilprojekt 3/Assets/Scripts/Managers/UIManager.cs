using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] List<Image> heartImages = new List<Image>();
    [SerializeField] TMP_Text lapText;

    public static UIManager instance;

    void Awake(){
        if(instance == null)
            instance = this;
        else Debug.LogWarning("big cry, 1< UI manager");
    }

    public void UpdateHeartsUI(int hearts){
        for(int h = 0; h < heartImages.Count; h++)
        {
            heartImages[h].enabled = (h < hearts);
        }
    }

    public void UpdateLapUI(int laps){
        lapText.enabled = (laps > 0);
        lapText.text = laps.ToString();
    }

   
}
