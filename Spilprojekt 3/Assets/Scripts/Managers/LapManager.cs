using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapManager : MonoBehaviour
{
    public static LapManager instance;
    public GameObject sinsContainer;

    //public SinBackStory sin;

    [Header("Lap colliders")]
    [SerializeField] Collider checkPoint1;
    [SerializeField] Collider checkPoint2;
    [SerializeField] Collider goal;

    private bool canCheckpoint1; // When last checkpoint was goal, checkpoint 1 can be triggered.
    private bool canCheckpoint2; // When last checkpoint was checkpoint 1, checkpoint 2 can be triggered.
    private bool canGoal; //When last checkpoint was checkpoint 2, goal can be triggered.

    [Header("Misc")]
    [SerializeField] private int laps;
    [SerializeField] private int lapsToWin;
    [SerializeField] UIManager ui;
    [SerializeField] Backstory winBackstory;

    void Awake(){
        if(instance == null)
            instance = this;
        else Debug.LogWarning("big cry, 1< lapmanager");

        ui.UpdateLapUI(laps);

        canCheckpoint1 = true;
    }

    public int GetLaps(){
        return laps;
    }

    public void ResetCheckpoints(){
        canGoal = false;
        canCheckpoint2 = false;
        canCheckpoint1 = true;
    }

    void IncreaseLaps(int amount){
        laps++;
        ui.UpdateLapUI(laps);
        if(lapsToWin <= laps){
            if(winBackstory!=null)
                GameManager.instance.SetStateToBackstory(winBackstory);
            else
                Debug.Log("No win backstory set in " + this.gameObject.name);
        }

        AddNewSin();        
    }

    public void CheckCollider(Collider col){
        if(col == goal){
            if(canGoal){
                canGoal = false;
                IncreaseLaps(1);
            }
            canCheckpoint1 = true;
            canCheckpoint2 = false;
        }
        if(col == checkPoint1){
            if(canCheckpoint1){
                canCheckpoint1 = false;
            }
            canCheckpoint2 = true;
            canGoal = false;
        }
        if(col == checkPoint2 && canCheckpoint2){
            canCheckpoint2 = false;
            canGoal = true;
        }
    }

<<<<<<< HEAD
    
=======
    public void AddNewSin()
    {
        // Gain random powerdown
        //Powerdown[] childs = sinsContainer.GetComponentsInChildren<Powerdown>();

        Debug.Log("Child objects: " + sinsContainer.transform.childCount);

        Powerdown[] availableSins = new Powerdown[sinsContainer.transform.childCount];
        int i = 0;
        foreach (Transform child in sinsContainer.transform)
        {
            Powerdown pd = child.gameObject.GetComponent<Powerdown>();

            if (pd == null)
            {
                Debug.LogWarning("Sin is missing Powerdown script: " + child.gameObject.name);
                continue;
            }

            if (!child.gameObject.activeInHierarchy)
            {
                availableSins[i] = pd;
            }
            i++;
        }

        // Get a random action powerdown
        if (availableSins.Length > 0)
        {
            Powerdown finalPowerdown = availableSins[Random.Range(0, availableSins.Length)];
            finalPowerdown.gameObject.SetActive(true);
        }
    }
>>>>>>> 86da202687abd04b1e420611d38e93c32744ba25
}
