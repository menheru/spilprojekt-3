using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;

    private AudioSource source;
    
    void Awake(){
        if(instance == null)
            instance = this;
    }

    void Start(){
        source = GetComponent<AudioSource>();
    }


    [Tooltip("Minecraft")]
    [SerializeField] AudioClip lavaSound;
    [Tooltip("gas gas gas")]
    [SerializeField] AudioClip boostSound;
    [Tooltip("skkrrrr")]
    [SerializeField] AudioClip hitSound;
    [Tooltip("Engine Fade, play once")]
    [SerializeField] AudioClip engineInitial;

    public void PlayLavaSound(){
        if(lavaSound != null)
            source.PlayOneShot(lavaSound);
        else
            Debug.Log("No lava sound");
    }
    public void PlayHitSound(){
        if(hitSound != null)
            source.PlayOneShot(hitSound);
        else
            Debug.Log("No hitSound");
    }
    public void PlayBoostSound(){
        if(boostSound != null)
            source.PlayOneShot(boostSound);
        else
            Debug.Log("No boostSound");
    }
    public void PlayEngineInitialSound()
    {
        if (engineInitial != null)
            source.PlayOneShot(engineInitial);
        else
            Debug.Log("No engineInitial");
    }


    void PlayClip(AudioClip clip){
        source.PlayOneShot(clip);
    }
}
