using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CarReferences
{
    public GameObject carParent;
    public Transform backWheels;
    public Transform frontWheels;
}
