using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    private GameManager gameManager;

    [SerializeField] Backstory introBackstory;

    private Fade fade;

    private void Start()
    {
        gameManager = GameManager.instance;

        fade = GetComponent<Fade>();

        fade.FadeIn();
    }

    public void StartGame()
    {
        fade.FadeOut();
        gameManager.SetStateToBackstory(introBackstory);
    }
}
