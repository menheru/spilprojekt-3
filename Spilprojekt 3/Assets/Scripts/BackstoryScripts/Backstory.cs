using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Backstory
{
    [Header("General")]
    [Tooltip("Prefab with animator and animation on it")]
    public GameObject BackstoryParent;
    
    [Header("Scene load")]
    [Tooltip("Enable this to load a scene after backstory was played. Specify scene in Scene Name variable below.")]
    public bool LoadSceneAfterBackstory;
    [Tooltip("Scene Name variable, this scene will be loaded if LoadSceneAfterBackstory is enabled.")]
    public string SceneName;
    [Header("Event")]
    [Tooltip("If checked event plays before backstory, if unchecked event plays after backstory.")]
    public bool playBefore;
    [Tooltip("Don't pause the game when event is played")]
    public bool dontPause;
    [Tooltip("If contains a event, event will be called on backstory was played.")]
    public VoidEvent Event;
    
}
