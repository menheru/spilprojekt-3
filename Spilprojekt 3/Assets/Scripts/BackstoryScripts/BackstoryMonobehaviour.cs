using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackstoryMonobehaviour : MonoBehaviour
{
    bool loadSceneAfterBackstory;
    string sceneToLoad;
    VoidEvent voidEvent;

    public void SetLoadScene(string _sceneToLoad){
        sceneToLoad = _sceneToLoad;
        loadSceneAfterBackstory = true;
    }

    public void SetVoidEvent(VoidEvent _event) => voidEvent = _event;

    public void BackstoryFinished(){
        GameManager.instance.SetStateToStarted();

        if(voidEvent != null) voidEvent.Raise();

        if(loadSceneAfterBackstory)
            SceneManager.LoadScene(sceneToLoad);
        else
            Destroy(this.gameObject);
    }
}
