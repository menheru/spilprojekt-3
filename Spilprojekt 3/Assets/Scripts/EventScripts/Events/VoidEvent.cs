using UnityEngine;

[CreateAssetMenu(fileName = "New VoidEvent", menuName = "Create VoidEvent")]
public class VoidEvent : BaseEvent<Void>
{
    public void Raise () => Raise(new Void());
}