using UnityEngine;
using System.Collections.Generic;

public abstract class BaseEvent<T> : ScriptableObject
{
    private readonly List<IEventListener<T>> eventListeners = new List<IEventListener<T>>();

    public void Raise(T item)
    {
        for(int i = eventListeners.Count - 1; i >= 0; i--)
        {
            eventListeners[i].OnEventRaised(item);
        }
    }

    public void RegisterListener(IEventListener<T> listener){
        if(!eventListeners.Contains(listener))
            eventListeners.Add(listener);

    }

    public void UnregisterListener(IEventListener<T> listener){
        if(eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
