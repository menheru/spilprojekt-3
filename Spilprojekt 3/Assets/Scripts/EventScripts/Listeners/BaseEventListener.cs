using UnityEngine;
using UnityEngine.Events;

public abstract class BaseEventListener<T, E, UER> : MonoBehaviour,
    IEventListener<T> where E : BaseEvent<T> where UER : UnityEvent<T>
{
    [SerializeField] private E gameEvent;
    public E Event {get {return gameEvent; } set {gameEvent = value;}}

    [SerializeField] private UER unityEventResponse;

    private void OnEnable()
    {
        if(gameEvent == null) {return;}
        
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        if(gameEvent == null) {return;}

        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T item){
        if(unityEventResponse != null)
        {
            unityEventResponse.Invoke(item);
        }
    }

}