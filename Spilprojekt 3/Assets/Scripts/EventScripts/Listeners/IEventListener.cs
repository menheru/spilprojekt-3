using UnityEngine;
using UnityEngine.Events;

public interface IEventListener<T>
{
    void OnEventRaised(T item);
}