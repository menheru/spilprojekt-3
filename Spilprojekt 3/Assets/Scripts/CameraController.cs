using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform target;

    [Range(0f, 1f)]
    [SerializeField] float smoothSpeed = 0;
    [SerializeField] Vector3 offset;

    [SerializeField] Vector3 lookOffset;

    void LateUpdate(){
        Vector3 desiredPostion = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp (transform.position, desiredPostion, smoothSpeed);
        
        

        transform.position = smoothedPosition;



        transform.LookAt(smoothedPosition);
    }

}
