 using UnityEngine;
using System.Collections;

/* Example script to apply trauma to the camera or any game object */
public class TraumaInducer : MonoBehaviour 
{
    [Tooltip("Seconds to wait before trigerring the explosion particles and the trauma effect")]
    [SerializeField]  float Delay = 1;
    [Tooltip("Maximum stress the effect can inflict upon objects Range([0,1])")]
    [SerializeField]  float MaximumStress = 0.6f;
    [Tooltip("Maximum distance in which objects are affected by this TraumaInducer")]
    [SerializeField]  float Range = 45;
    [Tooltip("Enable if you want to apply this TraumaInducer's shake to a StressReceiver on this object.")]
    [SerializeField] private bool objectHasStressReceiver = false; 
    [SerializeField] private Transform carPosition; 
    [SerializeField] private bool shakeOnStart = true;
    private IEnumerator Start()
    {
        if(shakeOnStart){
            /* Wait for the specified delay */
            yield return new WaitForSeconds(Delay);
            /* Play all the particle system this object has */
            PlayParticles();

            /* Find all gameobjects in the scene and loop through them until we find all the nearvy stress receivers */
            var targets = UnityEngine.Object.FindObjectsOfType<GameObject>();
            for(int i = 0; i < targets.Length; ++i)
            {
                var receiver = targets[i].GetComponent<StressReceiver>();
                if(receiver == null) continue;
                float distance = Vector3.Distance(transform.position, targets[i].transform.position);
                /* Apply stress to the object, adjusted for the distance */
                if(distance > Range) continue;
                float distance01 = Mathf.Clamp01(distance / Range);
                float stress = (1 - Mathf.Pow(distance01, 2)) * MaximumStress;
                receiver.InduceStress(stress);
            }

        }
    }

    private StressReceiver sr;
    public void Shake(){
        PlayParticles();
        if(objectHasStressReceiver)
        {
            sr = GetComponent<StressReceiver>();
            if(sr == null) return;
            if(carPosition == null) return;
            float distance = Vector3.Distance(transform.position, carPosition.position);
            /* Apply stress to the object, adjusted for the distance */
            if(distance > Range) return;
            float distance01 = Mathf.Clamp01(distance / Range);
            float stress = (1 - Mathf.Pow(distance01, 2)) * MaximumStress;
            sr.InduceStress(stress);
        }
        else{
            Debug.Log("Press that boolean");
        }
    }
    /* Search for all the particle system in the game objects children */
    private void PlayParticles()
    {
        var children = transform.GetComponentsInChildren<ParticleSystem>();
        for(var i  = 0; i < children.Length; ++i)
        {
            children[i].Play();
        }
        var current = GetComponent<ParticleSystem>();
        if(current != null) current.Play();
    }
}